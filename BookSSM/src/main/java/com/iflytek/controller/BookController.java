package com.iflytek.controller;

import com.github.pagehelper.PageInfo;
import com.iflytek.domain.Book;
import com.iflytek.service.InterfaceBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("/book")
public class BookController {

    @Autowired
    private InterfaceBookService interfaceBookService;

    @RequestMapping("/findAll")
    public String findAll(Model model){
        List<Book> books = interfaceBookService.findAll();
        model.addAttribute("books",books);
        return "list";
    }

    @RequestMapping("/addUI")
    public String addUI(){
        return "add";
    }

    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public String add(Book book){
        interfaceBookService.addBook(book);
        return "redirect:/book/findPage";
    }
    @RequestMapping("/removeById/{id}")
    public String removeById(@PathVariable int id){
        interfaceBookService.removeBookById(id);
        return "redirect:/book/findPage";
    }

    @RequestMapping("/modifyUI/{id}")
    public String modifyUI(@PathVariable int id,Model model){
        Book book = interfaceBookService.findById(id);
        model.addAttribute("book",book);
        return "update";
    }

    @RequestMapping("/modify")
    public String modify(Book book){
        interfaceBookService.updateBook(book);
        return "redirect:/book/findPage";
    }

    @RequestMapping(value = "/removeByIds",method = RequestMethod.POST)
    public String removeById(int[] ids){
        if(ids!=null){
            interfaceBookService.removeBooksByIds(ids);
        }
        return "redirect:/book/findPage";
    }

    @RequestMapping(value = "/findBookByCondition",method = RequestMethod.POST)
    public String findBookByCondition(Book book,Model model){
        List<Book> books = interfaceBookService.findBookByConditions(book);
        model.addAttribute("books",books);
        return "list";
    }

    @RequestMapping(value = "/findPage")
    public String findPage(Integer currentPage,Integer rows,Book book, Model model){
        if(currentPage==null){
            currentPage=1;
        }
        if (rows==null){
            rows=5;
        }
        PageInfo pageInfo = interfaceBookService.findPageBean(currentPage,rows,book);
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("searchMap",book);
        return "pagelist";
    }

}
