package com.iflytek.domain;

public class Book {
    //id int primary key auto_increment comment '书籍id',
    private Integer id;
    //name varchar(50) not null comment '书名',
    private String name;
    //author varchar(20) not null comment '作者',
    private String author;
    //quantity int not null comment '库存量',
    private Integer quantity;
    //status int not null comment '状态 0：待售，1：在售，2：停售'
    private Integer status;

    public Book() {
    }

    public Book(Integer id, String name, String author, Integer quantity, Integer status) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.quantity = quantity;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", quantity=" + quantity +
                ", status=" + status +
                '}';
    }
}
