package com.iflytek.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.iflytek.domain.Book;
import com.iflytek.mapper.InterfaceBookMapper;
import com.iflytek.service.InterfaceBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("bookService")
public class InterfaceBookServiceImpl implements InterfaceBookService {

    @Autowired
    private InterfaceBookMapper interfaceBookMapper;
    @Override
    public List<Book> findAll() {
        List<Book> bookList = interfaceBookMapper.select();
        return bookList;
    }

    @Override
    public void addBook(Book book) {
      interfaceBookMapper.insert(book);
    }

    @Override
    public void removeBookById(Integer id) {
        interfaceBookMapper.delete(id);
    }

    @Override
    public Book findById(Integer id) {
        return interfaceBookMapper.selectById(id);
    }

    @Override
    public void updateBook(Book book) {
        interfaceBookMapper.update(book);
    }

    @Override
    public void removeBooksByIds(int[] ids) {

        interfaceBookMapper.deleteByIds(ids);

    }

    @Override
    public List<Book> findBookByConditions(Book book) {
        return interfaceBookMapper.findBookByConditions(book);
    }
    @Override
    public PageInfo<Book> findPageBean(Integer currentPage, Integer rows, Book book){
        PageHelper.startPage(currentPage,rows);
        List<Book> books = interfaceBookMapper.findBookByConditions(book);
        PageInfo<Book> pageInfo=new PageInfo<Book>(books);
        return pageInfo;
    }


//    @Override
//    public PageBean findPageBean(Integer currentPage, Integer rows, Map<String, String[]> searchMap) {
//        if (currentPage<=0){
//            currentPage=1;
//        }
//        //1.创建空的PageBean对象
//        PageBean<Book> pageBean=new PageBean<Book>();
//        //2.设置当前页面属性和rows属性
//        pageBean.setCurrentPage(currentPage);
//        pageBean.setRows(rows);
//        //3.调用dao查询totalCount总记录数 dao.findTotalCount();
//        Integer totalCount=bookDao.findTotalCount(searchMap);
//        pageBean.setTotalCount(totalCount);
//        //4.start=(currentPage - 1)*rows
//        Integer start=(currentPage-1)*rows;
//        //5.调用dao查询list集合 dao.findByPage(int start, int rows)
//        List<Book> books=bookDao.findByPage(start,rows,searchMap);
//        pageBean.setList(books);
//        //6.计算总页码
//        Integer totalPage=totalCount%rows==0?totalCount/rows:totalCount/rows+1;
//        pageBean.setTotalPage(totalPage);
//        //7.返回PageBean对象
//        return pageBean;
//    }

}
