package com.iflytek.service;

import com.github.pagehelper.PageInfo;
import com.iflytek.domain.Book;

import java.util.List;
import java.util.Map;

/**
 * 书籍管理的业务接口
 */
public interface InterfaceBookService {

    /**
     * 查询所有书籍
     * @return 书籍列表
     */
    public List<Book> findAll();

    /**
     * 增加书籍
     * @param book 要新增的书
     */
    public void addBook(Book book);

    /**
     * 根据ID删出书籍
     * @param id 书籍id
     * @return 是否删除成功
     */
    public void removeBookById(Integer id);

    /**
     * 根据ID查询书籍
     * @param id 书籍id
     * @return 查询到的书籍
     */
    public Book findById(Integer id);

    /**
     * 根据书籍资料更新该信息
     * @param book 书籍信息
     * @return 更新是否成功
     */
    public void updateBook(Book book);

    /**
     *
     * @param ids
     * @return
     */
    public void removeBooksByIds(int[] ids);

    /**
     *分页条件查询
     * @param currentPage
     * @param rows
     * @param searchMap
     * @return
     */
    PageInfo<Book> findPageBean(Integer currentPage, Integer rows, Book book);

    List<Book> findBookByConditions(Book book);
}
