<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>列表页</title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/style.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/mycss.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/js/jquery-3.2.1.min.js"></script>
</head>
<body>
<div class="my-header">
    <h1><a href="">查询所有书籍信息</a></h1>
    <div class="left">
        <form class="form-inline" action="${pageContext.request.contextPath}/book/findBookByCondition" method="post">
            <div class="form-group">
                <label class="control-label" for="book-name">书名</label>
                <input class="form-control" type="text" id="book-name" name="name" placeholder="请输入邮箱..">
            </div>
            <div class="form-group">
                <label class="control-label" for="book-author">作者</label>
                <input class="form-control" type="text" id="book-author" name="author" placeholder="请输入密码..">
            </div>
            <div class="form-group">
                <label class="control-label" for="book-status">状态</label>
                <select name="status" id="book-status" class="form-control">
                    <option value="-1">请选择</option>
                    <option value="0">待售</option>
                    <option value="1">在售</option>
                    <option value="2">停售</option>
                </select>
            </div>
            <div class="form-group">
                <button class="btn btn-info" type="submit">查询</button>
            </div>
        </form>
    </div>
    <div class="right">
        <a class="btn btn-success" href="${pageContext.request.contextPath}/book/addUI">添加书籍</a>
        <a class="btn btn-success" href="javascript:deleteByIds();">删除选中</a>
    </div>
</div>
<div class="card-body">
    <form action="${pageContext.request.contextPath}/book/removeByIds" method="post" id="formIds">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>
                        <label class="lyear-checkbox checkbox-primary">
                            <input type="checkbox" id="check-all"><span></span>
                        </label>
                    </th>
                    <th>编号</th>
                    <th>书名</th>
                    <th>作者</th>
                    <th>库存量</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>

                <c:forEach items="${books}" var="book" varStatus="s">
                    <tr>
                        <td>
                            <label class="lyear-checkbox checkbox-primary">
                                <input type="checkbox" name="ids" value="${book.id}" class="ck-ids"><span></span>
                            </label>
                        </td>
                        <td>${s.count}</td>
                        <td>${book.name}</td>
                        <td>${book.author}</td>
                        <td>${book.quantity}</td>
                        <td>
                            <c:if test="${book.status==0}">
                                <font class="text-danger">
                                    待售
                                </font>
                            </c:if>
                            <c:if test="${book.status==1}">
                                <font class="text-success">
                                    在售
                                </font>
                            </c:if>
                            <c:if test="${book.status==2}">
                                <font class="text-gray">
                                    停售
                                </font>
                            </c:if>
                        </td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-xs btn-default" href="${pageContext.request.contextPath}/book/modifyUI/${book.id}" title="编辑" data-toggle="tooltip"><i class="mdi mdi-pencil"></i></a>
                                <a class="btn btn-xs btn-default" href="javascript:remove(${book.id})" title="删除" data-toggle="tooltip"><i class="mdi mdi-window-close"></i></a>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </form>

    <ul class="pagination">
        <li class="disabled"><span>«</span></li>
        <li class="active"><span>1</span></li>
        <li><a href="#1">2</a></li>
        <li><a href="#1">3</a></li>
        <li><a href="#1">4</a></li>
        <li class="disabled"><span>...</span></li>
        <li><a href="#!">»</a></li>
    </ul>

</div>
<script>
 function remove(obj){
     var flag=confirm("确认要删除该数据么？");
     if (flag){
         location.href="${pageContext.request.contextPath}/book/removeById/"+obj;
     }
 }
 function deleteByIds(){
     var isNotNull=$(".ck-ids:checked").length>0;
     if(!isNotNull){
         alert("至少请选择一个！");
     }else{
         var flag=confirm("确认要删除该数据么？");
         if (flag&&isNotNull){
             $("#formIds").submit();
         }
     }
 }
</script>
</body>
</html>
