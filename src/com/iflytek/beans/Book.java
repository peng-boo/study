package com.iflytek.beans;

public class Book {
    //书籍id,
    private Integer id;
    //书名,
    private String name;
    //作者,
    private String author;
    //库存量
    private Integer quantity;
    //状态 0待售，1在售，2停售
    private Integer status;

    public Book() {
    }

    public Book(Integer id, String name, String author, Integer quantity, Integer status) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.quantity = quantity;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", quantity=" + quantity +
                ", status=" + status +
                '}';
    }
}
