package com.iflytek.service.impl;

import com.iflytek.beans.Book;
import com.iflytek.beans.Page;
import com.iflytek.dao.InterfaceBookDao;
import com.iflytek.dao.impl.InterfaceBookDaoImpl;
import com.iflytek.service.InterfaceBookService;

import java.util.List;
import java.util.Map;

public class InterfaceBookServiceImpl implements InterfaceBookService {

    private InterfaceBookDao interfaceBookDao =new InterfaceBookDaoImpl();
    @Override
    public List<Book> findAll() {
        return interfaceBookDao.select();
    }

    @Override
    public boolean addBook(Book book) {
        return interfaceBookDao.insert(book)>0;
    }

    @Override
    public boolean removeBookById(Integer id) {
        return interfaceBookDao.delete(id)>0;
    }

    @Override
    public Book findById(Integer id) {
        return interfaceBookDao.select(id);
    }

    @Override
    public boolean updateBook(Book book) {
        return interfaceBookDao.update(book)>0;
    }

    @Override
    public void removeBooksByIds(String[] ids) {
        for (String id : ids) {
            interfaceBookDao.delete(Integer.parseInt(id));
        }
    }

    @Override
    public Page findPageBean(Integer currentPage, Integer rows, Map<String, String[]> searchMap) {
        if (currentPage<=0){
            currentPage=1;
        }
        //1.创建空的PageBean对象
        Page<Book> pageBean=new Page<Book>();
        //2.设置当前页面属性和rows属性
        pageBean.setCurrentPage(currentPage);
        pageBean.setRows(rows);
        //3.调用dao查询totalCount总记录数 dao.findTotalCount();
        Integer totalCount= interfaceBookDao.findTotalCount(searchMap);
        pageBean.setTotalCount(totalCount);
        //4.start=(currentPage - 1)*rows
        Integer start=(currentPage-1)*rows;
        //5.调用dao查询list集合 dao.findByPage(int start, int rows)
        List<Book> books= interfaceBookDao.findByPage(start,rows,searchMap);
        pageBean.setList(books);
        //6.计算总页码
        Integer totalPage=totalCount%rows==0?totalCount/rows:totalCount/rows+1;
        pageBean.setTotalPage(totalPage);
        //7.返回PageBean对象
        return pageBean;
    }
}
