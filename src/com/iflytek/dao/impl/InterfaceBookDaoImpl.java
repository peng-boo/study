package com.iflytek.dao.impl;

import com.iflytek.beans.Book;
import com.iflytek.dao.InterfaceBookDao;
import com.iflytek.utils.JdbcUtils;
import com.mysql.jdbc.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class InterfaceBookDaoImpl implements InterfaceBookDao {
    JdbcTemplate jt=new JdbcTemplate(JdbcUtils.getDataSource());
    @Override
    public List<Book> select() {
        String sql="select * from book_tb";
        List<Book> books = jt.query(sql, new BeanPropertyRowMapper<Book>(Book.class));
        return books;
    }

    @Override
    public Integer insert(Book book) {
        String sql="insert into book_tb values (null,?,?,?,?)";
        int count = jt.update(sql, book.getName(), book.getAuthor(), book.getQuantity(), book.getStatus());
        return count;
    }

    @Override
    public Integer delete(Integer id) {
        String sql="delete from book_tb where id=?";
        int count=jt.update(sql,id);
        return count;
    }

    @Override
    public Book select(Integer id) {
        String sql="select * from book_tb where id=?";
        List<Book> books = jt.query(sql,new BeanPropertyRowMapper<Book>(Book.class),id);
        if (books.size()>0){
            return books.get(0);
        }
        return null;
    }

    @Override
    public Integer update(Book book) {
        String sql="update book_tb set name=?,author=?,quantity=?,status=? where id=?";
        int count = jt.update(sql, book.getName(), book.getAuthor(), book.getQuantity(), book.getStatus(), book.getId());
        return count;
    }

    @Override
    public Integer findTotalCount(Map<String, String[]> searchMap) {
        //1定义模板sql
        String sql="select count(*) from book_tb where 1=1 ";
        StringBuilder sb=new StringBuilder(sql);
        Set<String> keys = searchMap.keySet();
        List<Object> params=new ArrayList<Object>();
        for (String key : keys) {
            if ("currentPage".equals(key)||"rows".equals(key)){
                continue;
            }
            String value=searchMap.get(key)[0];
            if ("status".equals(key)){
                if (!value.equals("-1")){
                    sb.append(" and "+key+" = ? ");
                    params.add(value);
                }
                continue;
            }
            if (!StringUtils.isNullOrEmpty(value)){
                sb.append(" and "+key+" like ? ");
                params.add("%"+value+"%");
            }
        }
        Integer count = jt.queryForObject(sb.toString(), Integer.class,params.toArray());
        return count;
    }

    @Override
    public List<Book> findByPage(Integer start, Integer rows, Map<String, String[]> searchMap) {
        String sql="select * from book_tb where 1=1  ";
        StringBuilder sb=new StringBuilder(sql);
        Set<String> keys = searchMap.keySet();
        List<Object> params=new ArrayList<Object>();
        for (String key : keys) {
            if ("currentPage".equals(key)||"rows".equals(key)){
                continue;
            }
            String value=searchMap.get(key)[0];
            if ("status".equals(key)){
                if (!value.equals("-1")){
                    sb.append(" and "+key+" = ? ");
                    params.add(value);
                }
                continue;
            }
            if (!StringUtils.isNullOrEmpty(value)){
                sb.append(" and "+key+" like ? ");
                params.add("%"+value+"%");
            }
        }

        //添加分页的查询
        sb.append(" limit ? , ? ");
        params.add(start);
        params.add(rows);

        List<Book> books = jt.query(sb.toString(), new BeanPropertyRowMapper<Book>(Book.class), params.toArray());
        return books;
    }

}
