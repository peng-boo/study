package com.iflytek.dao;

import com.iflytek.beans.Book;

import java.util.List;
import java.util.Map;

/**
 * 书籍管理的数据处理接口
 */
public interface InterfaceBookDao {
    /**
     * 查询所有书籍
     * @return
     */
    public List<Book> select();

    /**
     * 插入书籍
     * @param book 插入的书籍
     * @return 影响的行数
     */
    public Integer insert(Book book);

    /**
     * 根据id删除书籍
     * @param id 书籍id
     * @return 影响的行数
     */
    public Integer delete(Integer id);

    /**
     * 根据id查询书籍
     * @param id 书籍id
     * @return 查询到的书籍信息
     */
    public Book select(Integer id);

    /**
     * 根据书籍跟新书籍信息
     * @param book 书籍信息
     * @return 更新是否成功
     */
    public Integer update(Book book);

    Integer findTotalCount(Map<String, String[]> searchMap);

    List<Book> findByPage(Integer start, Integer rows, Map<String, String[]> searchMap);
}
