package com.iflytek.web.servlet;

import com.iflytek.service.InterfaceBookService;
import com.iflytek.service.impl.InterfaceBookServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ServletDeleteIds")
public class ServletDeleteIds extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String[] ids = request.getParameterValues("ids");
        if (ids!=null&&ids.length>0){
            InterfaceBookService interfaceBookService =new InterfaceBookServiceImpl();
            interfaceBookService.removeBooksByIds(ids);
        }
        response.sendRedirect(request.getContextPath()+"/ServletList");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
