package com.iflytek.web.servlet;

import com.iflytek.beans.Page;
import com.iflytek.service.InterfaceBookService;
import com.iflytek.service.impl.InterfaceBookServiceImpl;
import com.mysql.jdbc.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet("/ServletByPage")
public class ServletByPage extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        //1.接受请求参数
        String currentPage = request.getParameter("currentPage");
        String rows = request.getParameter("rows");

        if(StringUtils.isNullOrEmpty(currentPage)){
            currentPage="1";
        }
        if (StringUtils.isNullOrEmpty(rows)){
            rows="5";
        }
        //获取查询条件
        Map<String, String[]> searchMap = request.getParameterMap();
        //2.调用Service查询PageBean
        InterfaceBookService interfaceBookService =new InterfaceBookServiceImpl();
        Page pageBean= interfaceBookService.findPageBean(Integer.parseInt(currentPage),Integer.parseInt(rows),searchMap);
        //3.将PageBean存入request
        request.setAttribute("pageBean",pageBean);
        request.setAttribute("searchMap",searchMap);
        //4.转发list.jsp展示
        request.getRequestDispatcher("pagelist.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
