package com.iflytek.web.servlet;

import com.iflytek.beans.Book;
import com.iflytek.service.InterfaceBookService;
import com.iflytek.service.impl.InterfaceBookServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ServletToUpdate")
public class ServletToUpdate extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String id = request.getParameter("id");
        InterfaceBookService interfaceBookService =new InterfaceBookServiceImpl();
        Book book = interfaceBookService.findById(Integer.parseInt(id));
        if (book!=null){
            request.setAttribute("book",book);
            request.getRequestDispatcher("/update.jsp").forward(request,response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
