package com.iflytek.web.servlet;

import com.iflytek.beans.Book;
import com.iflytek.service.InterfaceBookService;
import com.iflytek.service.impl.InterfaceBookServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/ServletList")
public class ServletList extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        InterfaceBookService interfaceBookService =new InterfaceBookServiceImpl();
        List<Book> books = interfaceBookService.findAll();
        request.setAttribute("books",books);
        request.getRequestDispatcher("list.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
