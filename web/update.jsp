<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/materialdesignicons.min.css" rel="stylesheet">
    <link href="css/style.min.css" rel="stylesheet">
    <link href="css/mycss.css" rel="stylesheet">
</head>
<body>
<div class="my-header">
    <h1><a href="">修改书籍信息</a></h1>
</div>
<div class="my-content">
    <div class="card">
        <div class="card-body">
            <form class="form-horizontal" action="${pageContext.request.contextPath}/ServletUpdate" method="post">
                <div class="form-group">
                    <label class="col-xs-12" for="id-input">编号</label>
                    <div class="col-xs-12">
                        <input class="form-control" type="text" id="id-input" name="id" value="${book.id}" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-12" for="name-input">书名</label>
                    <div class="col-xs-12">
                        <input class="form-control" type="text" id="name-input" name="name" placeholder="书名.." value="${book.name}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-12" for="author-input">作者</label>
                    <div class="col-xs-12">
                        <input class="form-control" type="text" id="author-input" name="author" placeholder="作者.." value="${book.author}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-12" for="quantity-input">库存量</label>
                    <div class="col-xs-12">
                        <input class="form-control" type="text" id="quantity-input" name="quantity" placeholder="库存量.." value="${book.quantity}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-12">状态</label>
                    <div class="col-xs-12">
                        <label class="radio-inline" for="status-radio1">
                            <input type="radio" id="status-radio1" name="status" value="0" ${book.status==0?"checked":""}>
                            待售
                        </label>
                        <label class="radio-inline" for="status-radio2">
                            <input type="radio" id="status-radio2" name="status" value="1" ${book.status==1?"checked":""}>
                            在售
                        </label>
                        <label class="radio-inline" for="status-radio3">
                            <input type="radio" id="status-radio3" name="status" value="2" ${book.status==2?"checked":""}>
                            停售
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <button class="btn btn-primary" type="submit">提交</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>

</body>
</html>
