<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>列表页</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/materialdesignicons.min.css" rel="stylesheet">
    <link href="css/style.min.css" rel="stylesheet">
    <link href="css/mycss.css" rel="stylesheet">
    <script src="js/jquery-3.2.1.min.js"></script>
</head>
<body>
<div class="my-header">
    <h1><a href="">查询所有书籍信息</a></h1>
    <div class="left">
        <form class="form-inline" action="${pageContext.request.contextPath}/ServletByPage" method="get" >
            <div class="form-group">
                <label class="control-label" for="book-name">书名</label>
                <input class="form-control" type="text" id="book-name" name="name" placeholder="请输入书名.." value="${searchMap.name[0]}">
            </div>
            <div class="form-group">
                <label class="control-label" for="book-author">作者</label>
                <input class="form-control" type="text" id="book-author" name="author" placeholder="请输入作者.." value="${searchMap.author[0]}">
            </div>
            <div class="form-group">
                <label class="control-label" for="book-status">状态</label>
                <select name="status" id="book-status" class="form-control">
                    <option value="-1" ${searchMap.status[0]==-1?"selected":""}>全部</option>
                    <option value="0" ${searchMap.status[0]==0?"selected":""}>待售</option>
                    <option value="1" ${searchMap.status[0]==1?"selected":""}>在售</option>
                    <option value="2" ${searchMap.status[0]==2?"selected":""}>停售</option>
                </select>

            </div>
            <div class="form-group">
                <button class="btn btn-info" type="submit">查询</button>
            </div>
        </form>
    </div>
    <div class="right">
        <a class="btn btn-success" href="${pageContext.request.contextPath}/add.jsp">添加书籍</a>
        <a class="btn btn-success" href="javascript:deleteByIds();">删除选中</a>
    </div>
</div>
<div class="card-body">
    <form action="${pageContext.request.contextPath}/ServletDeleteIds" method="post" id="formIds">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>
                        <label class="lyear-checkbox checkbox-primary">
                            <input type="checkbox" id="check-all"><span></span>
                        </label>
                    </th>
                    <th>编号</th>
                    <th>书名</th>
                    <th>作者</th>
                    <th>库存量</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>

                <c:forEach items="${pageBean.list}" var="book" varStatus="s">
                    <tr>
                        <td>
                            <label class="lyear-checkbox checkbox-primary">
                                <input type="checkbox" name="ids" value="${book.id}"><span></span>
                            </label>
                        </td>
                        <td>${s.count}</td>
                        <td>${book.name}</td>
                        <td>${book.author}</td>
                        <td>${book.quantity}</td>
                        <td><font class="text-success">${book.status}</font></td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-xs btn-default" href="${pageContext.request.contextPath}/ServletToUpdate?id=${book.id}" title="编辑" data-toggle="tooltip"><i class="mdi mdi-pencil"></i></a>
                                <a class="btn btn-xs btn-default" href="javascript:remove(${book.id})" title="删除" data-toggle="tooltip"><i class="mdi mdi-window-close"></i></a>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </form>

    <ul class="pagination">
        <c:if test="${pageBean.currentPage==1}">
            <li class="disabled">
                <span>«</span>
            </li>
        </c:if>
        <c:if test="${pageBean.currentPage!=1}">
            <li>
                <a href="${pageContext.request.contextPath}/ServletByPage?currentPage=${pageBean.currentPage-1}&rows=5&name=${searchMap.name[0]}&author=${searchMap.author[0]}&status=${searchMap.status[0]}">«</a>
            </li>
        </c:if>

        <c:forEach begin="1" end="${pageBean.totalPage}" var="i">
            <c:if test="${pageBean.currentPage==i}">
                <li class="active"><a href="${pageContext.request.contextPath}/ServletByPage?currentPage=${i}&rows=5&name=${searchMap.name[0]}&author=${searchMap.author[0]}&status=${searchMap.status[0]}">${i}</a></li>
            </c:if>
            <c:if test="${pageBean.currentPage!=i}">
                <li><a href="${pageContext.request.contextPath}/ServletByPage?currentPage=${i}&rows=5&name=${searchMap.name[0]}&author=${searchMap.author[0]}&status=${searchMap.status[0]}">${i}</a></li>
            </c:if>
        </c:forEach>

        <c:if test="${pageBean.currentPage==pageBean.totalPage}">
            <li class="disabled">
                <span> » </span>
            </li>
        </c:if>
        <c:if test="${pageBean.currentPage!=pageBean.totalPage}">
            <li>
                <a href="${pageContext.request.contextPath}/ServletByPage?currentPage=${pageBean.currentPage+1}&rows=5&name=${searchMap.name[0]}&author=${searchMap.author[0]}&status=${searchMap.status[0]}"> » </a>
            </li>
        </c:if>
        <li><span><b>共${pageBean.totalCount}条记录，共${pageBean.totalPage}页</b></span></li>
    </ul>

</div>
<script>
    function remove(obj){
        var flag=confirm("确认要删除该数据么？");
        if (flag){
            location.href="${pageContext.request.contextPath}/ServletDelete?id="+obj;
        }
    }
    function deleteByIds(){
        var flag=confirm("确认要删除该数据么？");
        if (flag){
            $("#formIds").submit();
        }

    }
</script>
</body>
</html>
